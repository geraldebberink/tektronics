% Copyright (C) 2005, Gerald Ebberink <g.h.p.ebberink at utwente dot nl>
% This program is free software; you can redistribute it and/or modify
%
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
%
% reads the csv's made by tektronix osciloscopes
%
% Useage:   data = Tekread(datadir)
% datadir is a string which gives the relative directory which is to be read
% data is structure returned with several members
%  data.Waveforms is the data of channels 
%  data.Time is the time vector
%  data.Index is a vector which has the same length as data.waveforms and can be used to shift the figures a little
% Author: Gerald Ebberink <g.h.p.ebberink at utwente dot nl>
% Version: 1.1.0
% Keywords: file I/O tektronics
% Last Change: november 2012

function data = Tekread(datadir)

% if the directory ends with a slash 
% kill the slash, this is usefull later on
if datadir(end)=='/'
  datadir=datadir(1:end-1);
end;
	
% The filenames in the data dir are numbered by the osciloscope
% to get the right number the folowing is done

filename = ls(datadir);

% The first two are . and .. so we skip those
fileprepend = filename(3,1:7);

for c=1:8 % for the maximum of 8 channels
    filename = sprintf('%s/%s%i.csv',datadir,fileprepend,c); 
    if exist(filename,'file') % if file exists
      fid = fopen(filename,'rt'); %open channel file
      str = fgetl(fid); %get first line
      dat=[]; % reset data structure
      Time=[]; % reset time structure
      i=0;
      while str~=-1
        i=i+1;
        idx = strfind(str,',');
		
        if strfind(str,'Vertical Units') % search for the unit and put it in the data set
          data.VerticalUnit(c,:) = str(idx(1)+1:idx(2)-1);
        end;
      
        if strfind(str,'Vertical Offset') % search for the Offset number and again put it in the data set
          data.VerticalOffset(c) = str2double(str(idx(1)+1:idx(2)-1));
        end;				
 
        if strfind(str,'Vertical Scale') % search for the Vertical scale and put it in the data set
          data.VerticalScale(c) = str2double(str(idx(1)+1:idx(2)-1));
        end;
        
        if strfind(str,'Horizontal Scale') % search for the Horizontal scale 
            data.HorizontalScale = str2double(str(idx(1)+1:idx(2)-1));
        end;
			
        if strfind(str, 'Horizontal Units') % find the Horizontal Units
            data.HorizontalUnit = str(idx(1)+1:idx(2)-1);
        end;
        
        if strfind(str, 'Sample Interval') % find the Horizontal Units
            data.SampleInterval = str2double(str(idx(1)+1:idx(2)-1));
        end;
        
        if strfind(str, 'Record Length') % find the number of records
          data.RecordLength = str2double(str(idx(1)+1:idx(2)-1));
          Time = zeros(1,data.RecordLength);
          dat = zeros(1,data.RecordLength);
        end
        
        Time(i) = str2double(str(idx(3)+1:idx(4)-1)); % Put the time data in the time array
        dat(i) = str2double(str(idx(4)+1:end-1)); % put the waveform data in the data structure
        str = fgetl(fid);
      end;
      data.Waveforms(c,:)=dat;
      data.Time=Time;
      fclose(fid);  
    end;
end;
  
% Make a sample index (this is usefull in other programs)
data.Index=[1:1:data.RecordLength];




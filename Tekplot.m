% Copyright (C) 2012, Gerald Ebberink <g.h.p.ebberink at utwente dot nl>
% This program is free software; you can redistribute it and/or modify
%
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

% plots graphs from a data structure created by Tekread

% Useage:   Tekplot(data)

% Author: Gerald Ebberink <g.h.p.ebberink at utwente dot nl>
% Version: 0.0.1
% Keywords: graph plot tektronics
% Last Change: november 2012

function Tekplot(data)
figure()
plot(data.Time,data.Waveforms);
title('osciloscope results');
xlabel(sprintf('Time [%s]',data.HorizontalUnit));
ylabel(sprintf('Voltage [%s]',data.VerticalUnit));
legend SHOW;
figure();
NFFT = 2^nextpow2(data.RecordLength); % Next power of 2 from length of y
Fs = 1/data.SampleInterval; 
Y = fft(data.Waveforms,NFFT)/data.RecordLength;
f = Fs/2*linspace(0,1,NFFT/2+1);
plot(f(1:end),2*abs(Y(1:NFFT/2+1))) 
title('Single-Sided Amplitude Spectrum of V(t)')
xlabel('Frequency (Hz)')
ylabel('|V(f)|')
end
